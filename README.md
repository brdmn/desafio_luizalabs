# Luiza Labs Challenge

### The Problem

Luizalabs' team is growing every month and now we need to have some application to manage employees' information, such as name, e-mail and department. As any application written at Luizalabs, it must have an API to allow integrations. 

#### Deliverables "Luizalabs Employee Manager" app must have:

    1. A Django Admin panel to manage employees' data
    2. An API to list, add and remove employees

### Prerequisites :

    1. Unix Shell
    2. Python >= 2.7 
    3. Python packages
        To install packages you can run:
            1. pip install -r requirements.txt

### API Routes: 

 Route                     |     HTTP Method   |      Description      | 
-------------------------  | ----------------- | --------------------- | 
/employees                 |       GET         | List all employees    | 
/employees                 |       POST        | Create an employee    | 
/employees/:id             |       GET         | List an employee      | 
/employees/:id/            |       PUT         | Update an employee    |    
/employees/:id/            |       DELETE      | Delete an employee    |



### Running API Service:

    1. cd desafio_luizalabs
    2. python manage.py runserver 
    
### Database

    1. This project contains a sqlite3 database with a little data sample. If you preferer, you can delete this database and create another one. Remember to run django migrations and create a superuser.


### Authentication

Authentication is necessary for all requests. Superuser has more privileges.

    **Admin user** (all privileges)(GET, POST, PUT, DELETE) 
        login : admin 
        password: labs@2018

    **Normal user** (read-only)(GET) 
        login : normal_user
        password : labs@123

    
### Browser mode

    **Accessing docs**
        1. Access http://127.0.0.1:8000 & Authenticate
    
    **Accessing Online API Tool**
        1. Access http://127.0.0.1:8000/employees/
                        or
        2. Access http://127.0.0.1:8000/employees/id
    
### Curl mode

    curl - <HTTP-METHOD> http://127.0.0.1:8000/employees/ -u login:password


### Tests:
    
    1. cd desafio_luizalabs
    2. python manage.py test