# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Employee(models.Model):

    name = models.CharField(max_length=300)
    email = models.EmailField()
    department = models.CharField(max_length=300)
