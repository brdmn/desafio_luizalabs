# -*- coding: utf-8 -*-


from rest_framework import permissions


class EmployeePermission(permissions.BasePermission):

    def has_permission(self, request, view):

        if request.method == 'GET':
            return request.user.is_authenticated()
        else:
            return request.user.is_authenticated and request.user.is_superuser
