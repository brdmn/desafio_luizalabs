# -*- coding: utf-8 -*-

from django.urls import reverse
from django.contrib.auth.models import User

from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.test import APIClient

from employees.models import Employee


class EmployeesTests(APITestCase):

    def setUp(self):

        tester1 = User.objects.create_user(username='tester',
                                           password='tester@tester')
        tester1.is_superuser = True
        tester1.is_staff = True
        tester1.save()

        tester2 = User.objects.create_user(username='tester2',
                                           password='tester2@tester2')
        tester2.save()

        # Admin client
        self.client1 = APIClient()
        self.client1.login(username='tester',
                           password='tester@tester')

        # Normal client
        self.client2 = APIClient()
        self.client2.login(username='tester2',
                           password='tester2@tester2')

        # No login
        self.client3 = APIClient()

    def test_create_employee(self):
        """
        Ensure we can create a new employee object.
        """

        url = reverse('list_employees')

        data = {'name': 'user_test', 'email': 'user_test@luizalabs.com',
                'department': 'tester'}

        response = self.client2.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Employee.objects.count(), 1)
        self.assertEqual(Employee.objects.get().name, 'user_test')

    def test_list_employees(self):
        """
        Ensure we can list employees.
        """

        url = reverse('list_employees')

        response = self.client2.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_employee(self):
        """
        Ensure we can delete an employee.
        """

        url = reverse('update_employees', kwargs={'pk': 1})

        # Creating an employee
        self.test_create_employee()

        # Checking if employee was created
        self.assertEqual(Employee.objects.count(), 1)

        # Testing user without authorization
        response = self.client2.delete(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(Employee.objects.count(), 1)

        # Testing user with authorization
        response = self.client1.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Employee.objects.count(), 0)
