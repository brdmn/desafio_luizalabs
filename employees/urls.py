# -*- coding: utf-8 -*-

from django.conf.urls import url
from django.views.generic.base import RedirectView

from employees.views import EmployeeList, EmployeeDetail

urlpatterns = [
    url(r'^$', RedirectView.as_view(url='/api-auth/login/')),
    url(r'^employees/$', EmployeeList.as_view(), name='list_employees'),
    url(r'^employees/(?P<pk>[0-9]+)/$',
        EmployeeDetail.as_view(), name='update_employees'),
]
