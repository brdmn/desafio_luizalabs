# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from rest_framework import generics
from rest_framework import permissions

from employees.models import Employee
from employees.permissions import EmployeePermission
from employees.serializers import EmployeeSerializer


class EmployeeList(generics.ListCreateAPIView):
    """
    Return a list of all the existing employees and create
    a new employee.

    get:
    Return all employees.

    post:
    Create a new employee.
    """

    queryset = Employee.objects.all().order_by('name')
    serializer_class = EmployeeSerializer
    permission_classes = (EmployeePermission,)


class EmployeeDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Manipulate an employee.

    get:
    Return an employee (per id).

    put:
    Update an employee.

    patch:
    Partial update an employee.

    delete:
    Delete an employee.
    """

    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer
    permission_classes = (EmployeePermission,)
