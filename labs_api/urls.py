# -*- coding: utf-8 -*-

from django.contrib import admin
from django.conf.urls import url, include

from rest_framework.documentation import include_docs_urls


urlpatterns = [
    url(r'^', include('employees.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls',
                               namespace='rest_framework')),
    url(r'^docs/', include_docs_urls(title='Luiza Labs API',
                                     description='''
                                     API to manage Luiza Labs employees
                                     information, such as name, e-mail
                                     and department.''',
                                     public=False))
]
